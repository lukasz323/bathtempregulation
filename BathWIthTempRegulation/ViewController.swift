//
//  ViewController.swift
//  BathWIthTempRegulation
//
//  Created by Lukasz on 2014-12-04.
//  Copyright (c) 2014 Lukasz Gawryjolek. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var hotTapImageView: UIImageView!
	@IBOutlet weak var coldTapImageView: UIImageView!
	@IBOutlet weak var bathWaterTemperatureLabel: UILabel!
	@IBOutlet weak var bathWaterAmountLabel: UILabel!
	@IBOutlet weak var dialImageView: UIImageView!
	@IBOutlet weak var hotLabel: UILabel!
	@IBOutlet weak var coldLabel: UILabel!
	@IBOutlet weak var hotTemperatureLabel: UILabel!
	@IBOutlet weak var coldTemperatureLabel: UILabel!
	
	let bath: Bath = Bath(bathWaterAmount: 0.0, bathWaterTemperature: 0.0)
	let bathWaterAmountTagView = 101
	
	@IBAction func emptyBath(sender: AnyObject) {
		
		//animation
		var view = self.view.viewWithTag(self.bathWaterAmountTagView)
		if view != nil {
			let rect = CGRect(x: self.view.bounds.origin.x, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: 0)
			UIView.animateWithDuration(2.0,
				delay: 0.0,
				options: UIViewAnimationOptions.CurveLinear,
				animations: {
					view!.bounds = rect
				}, completion: nil)
		}
		
		bath.emptyBath()
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		self.view.backgroundColor = UIColor(patternImage:UIImage(named: "Background")!)
		
		//add tap gestures to imageView
		let hotTapGesture = UITapGestureRecognizer(target: self, action: "hotTapClick")
		hotTapImageView.addGestureRecognizer(hotTapGesture)
		hotTapImageView.userInteractionEnabled = true
		
		let coldTapGesture = UITapGestureRecognizer(target: self, action: "coldTapClick")
		coldTapImageView.addGestureRecognizer(coldTapGesture)
		coldTapImageView.userInteractionEnabled = true
		
		
		//update UI
		updateBathParameters()
		updateTapPositions()
		updateTapsTemperature()
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		//addObservers
		let notification = NSNotificationCenter.defaultCenter()
		notification.addObserver(self, selector: "updateBathParameters", name: BathWaterAmountChangedNotification, object: nil)
		notification.addObserver(self, selector: "updateBathParameters", name: BathWaterTemperatureChangedNotification, object: nil)
		
		notification.addObserver(self, selector: "updateTapPositions", name: HotTapChangedNotification, object: nil)
		notification.addObserver(self, selector: "updateTapPositions", name: ColdTapChangedNotification, object: nil)
		
		notification.addObserver(self, selector: "bathReachMaximumAmount", name: BathWaterMaximumAmountNotification, object: nil)
		
		notification.addObserver(self, selector: "updateTapsTemperature", name: ColdHotTemperatureChangedNotification, object: nil)
		
	}
	
	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		
		//remove observers
		let notification = NSNotificationCenter.defaultCenter()
		notification.removeObserver(self, name: BathWaterAmountChangedNotification, object: nil)
		notification.removeObserver(self, name: BathWaterTemperatureChangedNotification, object: nil)
		
		notification.removeObserver(self, name: HotTapChangedNotification, object: nil)
		notification.removeObserver(self, name: ColdTapChangedNotification, object: nil)
		
		notification.removeObserver(self, name: BathWaterMaximumAmountNotification, object: nil)
		notification.removeObserver(self, name: ColdHotTemperatureChangedNotification, object: nil)
	}
	
	func hotTapClick() {
		bath.hotTapOpen = !bath.hotTapOpen
	}
	func coldTapClick() {
		bath.coldTapOpen = !bath.coldTapOpen
	}
	
	func updateBathParameters() {
		NSOperationQueue.mainQueue().addOperationWithBlock(){
			//bath temperature UI
			self.bathWaterTemperatureLabel.text = NSString(format: "%0.1f °C", self.bath.bathWaterTemperature)
			self.rotateDial()
			
			//bath amount UI
			self.bathWaterAmountLabel.text = NSString(format: "%0.1f Liters", self.bath.bathWaterAmount)
			
			let hView = self.bath.bathWaterAmount / self.bath.bathWaterMaxAmount * Double(self.view.bounds.size.height)
			let deltaH = self.view.bounds.height - CGFloat(hView)
			var rect = self.view.bounds
			rect.size.height = CGFloat(hView)
			rect.origin = CGPointMake(0, deltaH)
			var view = self.view.viewWithTag(self.bathWaterAmountTagView)
			
			//create new view
			if  view == nil {
				view = UIView(frame: rect)
				view!.tag = self.bathWaterAmountTagView
				view!.backgroundColor = UIColor.blueColor()
				view!.alpha = 0.4
				self.view.insertSubview(view!, atIndex: 0)
			}
			view!.frame = rect
		}
		
	}
	
	private func rotateDial() {
		let maxScale = 60.0 //celcius
		let alfa = ( (M_PI / maxScale) * bath.bathWaterTemperature) - M_PI_2
		var transform: CGAffineTransform = CGAffineTransformIdentity
		transform = CGAffineTransformTranslate(transform, 0, +25)
		transform = CGAffineTransformRotate(transform, CGFloat(alfa))
		transform = CGAffineTransformTranslate(transform, 0, -25)
		dialImageView.transform = transform
	}
	
	
	func updateTapPositions() {
		NSOperationQueue.mainQueue().addOperationWithBlock(){
			
			var transform: CGAffineTransform = CGAffineTransformIdentity
			
			//hot tap
			if self.bath.hotTapOpen {
				
				//animation
				transform = CGAffineTransformRotate(transform, CGFloat(M_PI_2/2.0))
				transform = CGAffineTransformScale(transform, 1.2, 1.2)
				UIView.animateWithDuration(0.3,
					delay: 0.0,
					options: (.BeginFromCurrentState | .AllowUserInteraction | .CurveEaseInOut),
					animations: {
						self.hotTapImageView.transform = transform
					}, completion: {(Bool) -> Void in
						self.hotLabel.textColor = UIColor.redColor()
				})
			} else {
				
				//animation
				UIView.animateWithDuration(0.3,
					delay: 0.0,
					options: (.BeginFromCurrentState | .AllowUserInteraction | .CurveEaseInOut),
					animations: {
						self.hotTapImageView.transform = CGAffineTransformIdentity
					}, completion: {(Bool) -> Void in
						self.hotLabel.textColor = UIColor.blackColor()
				})
			}
			
			transform = CGAffineTransformIdentity
			//cold tap
			if self.bath.coldTapOpen {
				
				//animation
				transform = CGAffineTransformRotate(transform, CGFloat(M_PI_2/2.0))
				transform = CGAffineTransformScale(transform, 1.2, 1.2)
				UIView.animateWithDuration(0.3,
					delay: 0.0,
					options: (.BeginFromCurrentState | .AllowUserInteraction | .CurveEaseInOut),
					animations: {
						self.coldTapImageView.transform = transform
					}, completion: {(Bool) -> Void in
						self.coldLabel.textColor = UIColor.blueColor()
				})
			} else {
				
				//animation
				UIView.animateWithDuration(0.3,
					delay: 0.0,
					options: (.BeginFromCurrentState | .AllowUserInteraction | .CurveEaseInOut),
					animations: {
						self.coldTapImageView.transform = CGAffineTransformIdentity
					}, completion: {(Bool) -> Void in
						self.coldLabel.textColor = UIColor.blackColor()
				})
			}
		}
	}
	
	func bathReachMaximumAmount() {
		let bathWaterBackgroundView = self.view.viewWithTag(self.bathWaterAmountTagView)!
		let orginalColor = bathWaterBackgroundView.backgroundColor
		UIView.animateWithDuration(2.0,
			delay: 0.0,
			options: UIViewAnimationOptions.CurveLinear,
			animations: {
				bathWaterBackgroundView.backgroundColor = UIColor.redColor()
			}, completion: {(Bool) -> Void in
				bathWaterBackgroundView.backgroundColor = orginalColor
		})
	}
	
	func updateTapsTemperature() {
		//hot water
		var hotTemperatureString = "??? °C"
		if let hotTemperature = bath.hotWaterTemperature {
			hotTemperatureString = NSString(format: "%0.1f °C", hotTemperature)
		}
		self.hotTemperatureLabel.text = hotTemperatureString
		
		//cold water
		var coldTemperatureString = "??? °C"
		if let coldTemperature = bath.coldWaterTemperature {
			coldTemperatureString = NSString(format: "%0.1f °C", coldTemperature)
		}
		self.coldTemperatureLabel.text = coldTemperatureString
		
	}
}

