//
//  ServerAPI.swift
//  BathWithTempRegulation
//
//  Created by Lukasz on 2014-12-06.
//  Copyright (c) 2014 Lukasz Gawryjolek. All rights reserved.
//

import Foundation

typealias ServerDataResponseErrorCompletionHandler = (data: NSData!, response:  NSURLResponse!, error: NSError!) -> Void
typealias ColdAndHotWaterTemperature = (cold: Double?, hot: Double?)
/**
* Network API to Server
*/
class ServerAPI: NSObject {
	
	var request = NSMutableURLRequest(URL: NSURL(string: "http://localhost:8000/bath.json")!)
	var session = NSURLSession.sharedSession()

	override init() {
		request.HTTPMethod = "GET"
		request.timeoutInterval = 10
		request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData
	}
	
	/*!
	 * Get from Server temperatures information
	 * @param serverResponse Server Completion Handler with return data and error if occured
	 */
	func readFromServerTemperatures(serverResponse: ServerDataResponseErrorCompletionHandler) {
		let task = session.dataTaskWithRequest(request, completionHandler: serverResponse)
		task.resume()
	}
	
	/*!
	* Parse JSON data
	* @param data JSON data to parse
	* @return Tuple (Double?, Double?) with cold and hot water temperature
	*/
	func parseToTemperatures(data: NSData) -> (ColdAndHotWaterTemperature) {
		var jsonError: NSError?
		let parsedJSON: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &jsonError)
		var temperatures: ColdAndHotWaterTemperature = (nil, nil)
		
		//JSON parsing
		if let parsed = parsedJSON as? [String:AnyObject] {
			let coldWater = parsed["cold_water"] as NSNumber?
			let hotWater = parsed["hot_water"] as NSNumber?
			temperatures = (coldWater?.doubleValue, hotWater?.doubleValue)
		} else {
			println("Parsing error, JSON structure is wrong!")
		}
		return temperatures
	}
}
