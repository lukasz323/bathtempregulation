//
//  Bath.swift
//  BathWIthTempRegulation
//
//  Created by Lukasz on 2014-12-04.
//  Copyright (c) 2014 Lukasz Gawryjolek. All rights reserved.
//

import Foundation

//Notification names constants to let UI about Model change
let BathWaterAmountChangedNotification		= "BathWaterAmountChangedNotification"
let BathWaterTemperatureChangedNotification = "BathWaterTemperatureChangedNotification"
let HotTapChangedNotification				= "HotTapChangedNotification"
let ColdTapChangedNotification				= "ColdTapChangedNotification"
let BathWaterMaximumAmountNotification		= "BathWaterMaximumAmountNotification"
let ColdHotTemperatureChangedNotification	= "ColdHotTemperatureChangedNotification"


/**
* Model of (MVC)- Bath with water in it and two taps (cold, hot) water
*/
class Bath : NSObject {
	
	/**
	* Time interval for which flow of water and temperature is calculated in minutes
	* ex. 100ms
	*/
	private let timeInterval: Double = 1/60/10
	
	/**
	* Timer that controls work of a repeating counter for time interval
	*/
	private var timeCounter: NSTimer?
	
	/**
	* Amount of water in the bath in litres
	*/
	private(set) var bathWaterAmount: Double = 0.0 {
		didSet {
			if bathWaterAmount != oldValue {
				NSNotificationCenter.defaultCenter().postNotificationName(BathWaterAmountChangedNotification, object: nil)
			}
		}
	}
	
	/**
	* Temperatur of water in the bath in Celsuis
	*/
	private(set) var bathWaterTemperature: Double = 0.0 {
		didSet {
			if bathWaterTemperature != oldValue {
				NSNotificationCenter.defaultCenter().postNotificationName(BathWaterTemperatureChangedNotification, object: nil)
			}
		}
	}
	
	/**
	* Bath maximum capacity, when bath reaches this amount of water both taps are close automatically
	*/
	let bathWaterMaxAmount: Double = 15

	/**
	* Open cold tap when set to true
	*/
	var coldTapOpen: Bool = false {
		didSet {
			if coldTapOpen != oldValue {
				checkTaps()
				NSNotificationCenter.defaultCenter().postNotificationName(ColdTapChangedNotification, object: nil)
			}
		}
	}
	
	/**
	* Open hot tap when set to true
	*/
	var hotTapOpen: Bool = false {
		didSet {
			if hotTapOpen != oldValue {
				checkTaps()
				NSNotificationCenter.defaultCenter().postNotificationName(HotTapChangedNotification, object: nil)
			}
		}
	}
	
	/**
	* Constant flow of cold water in litres/minutes
	*/
	let coldWaterFlow: Double = 12.0
	
	/**
	* Constant flow of hot water in litres/minutes
	*/
	let hotWaterFlow: Double = 10.0
	

	/**
	* Current cold water termperature
	*/
	private(set) var coldWaterTemperature: Double? = nil {
		didSet {
			if coldWaterTemperature != oldValue {
				NSNotificationCenter.defaultCenter().postNotificationName(ColdHotTemperatureChangedNotification, object: nil)
			}
		}
	}
	

	/**
	* Current hot water termperature
	*/
	private(set) var hotWaterTemperature: Double? = nil {
		didSet {
			if hotWaterTemperature != oldValue {
				NSNotificationCenter.defaultCenter().postNotificationName(ColdHotTemperatureChangedNotification, object: nil)
			}
		}
	}

	
	/**
	* Current hot water termperature
	*/
	private let severAPI = ServerAPI()
	
	/**
	* Count failed network retries to get water termperatures
	*/
	var networkRetries: NSInteger = 0 {
		didSet {
			if networkRetries > 3 {
				//server not available start simulation
				self.coldWaterTemperature = 10.0
				self.hotWaterTemperature = 50.0
			}
		}
	}

	//designated initializer
	init(bathWaterAmount: Double, bathWaterTemperature: Double) {
		self.bathWaterAmount = bathWaterAmount
		self.bathWaterTemperature = bathWaterTemperature
		
		super.init()
		
		//initialize reading cold and hot water temperature from server
		updateColdAndHotWater();
	}
	
	//convenience initializer use for UnitTest
	convenience init(bathWaterAmount: Double, bathWaterTemperature: Double,
		hotWaterTemperature: Double, coldWaterTemperature: Double) {
			self.init(bathWaterAmount: bathWaterAmount, bathWaterTemperature: bathWaterTemperature)
			
			self.hotWaterTemperature = hotWaterTemperature
			self.coldWaterTemperature = coldWaterTemperature
	}
	
	/**
	* Calculated amount of cold water in litres that comes to bath within time interval
	*/
	var coldWaterAmountForTimeInterval: Double {
		get {
			let openFactor = coldTapOpen ? 1 : 0
			return coldWaterFlow * timeInterval * Double(openFactor)
		}
	}
	
	/**
	* Calculated amount of how water in litres that comes to bath within time interval
	*/
	var hotWaterAmountForTimeInterval: Double {
		get {
			let openFactor = hotTapOpen ? 1 : 0
			return hotWaterFlow * timeInterval * Double(openFactor)
		}
	}
	
	private func checkTaps() {
		//start counter if it is not started yet
		if (coldTapOpen || hotTapOpen) && (timeCounter == nil) {
			timeCounter = NSTimer.scheduledTimerWithTimeInterval(timeInterval*60.0,
			target: self,
			selector: "timeIntervalPassed:",
			userInfo: nil,
			repeats: true)
		} else if !coldTapOpen && !hotTapOpen && (timeCounter != nil) {
			//if both taps are close stops timer
			closeAllTaps()
		}
	}
	
	//trigger by timer
	func timeIntervalPassed(timer: NSTimer) {
		calculate()
	}
	
	/**
	* Calculate the bath thermodynamic temperature bilans
	*/
	private func calculate()  {
		let newBathWaterAmount = coldWaterAmountForTimeInterval + hotWaterAmountForTimeInterval + bathWaterAmount
		
		//calculation makes sense there is any water in the bath
		if newBathWaterAmount != 0.0 {
			if coldWaterTemperature != nil && hotWaterTemperature != nil {
				var newBathTemperature: Double = 0.0
				newBathTemperature += bathWaterAmount					* convertCelciusToKelvin(bathWaterTemperature)
				newBathTemperature += coldWaterAmountForTimeInterval	* convertCelciusToKelvin(coldWaterTemperature!)
				newBathTemperature += hotWaterAmountForTimeInterval		* convertCelciusToKelvin(hotWaterTemperature!)
				bathWaterTemperature = convertKelvinToCelcius(newBathTemperature / newBathWaterAmount)
				bathWaterAmount = newBathWaterAmount

				println("C: \(coldWaterAmountForTimeInterval),t= \(coldWaterTemperature!),H: \(hotWaterAmountForTimeInterval),t= \(hotWaterTemperature!)")
				println("L: \(newBathTemperature),M: \(newBathWaterAmount)t= \(bathWaterTemperature)")
				println()
				checkBathOverflow()
			} else  {
				//cold or how water temperature is not know - close taps
				closeAllTaps()
				
				updateColdAndHotWater();
				
				NSNotificationCenter.defaultCenter().postNotificationName(ColdHotTemperatureChangedNotification, object: nil)
			}
		}
	}
	
	private func checkBathOverflow() {
		if bathWaterAmount >= bathWaterMaxAmount {
			NSNotificationCenter.defaultCenter().postNotificationName(BathWaterMaximumAmountNotification, object: nil)
		
			closeAllTaps()
			
		}
	}
	
	private func closeAllTaps() {
		hotTapOpen = false
		coldTapOpen = false
		timeCounter?.invalidate()
		timeCounter = nil

	}
	/**
	* Empty bath, and close taps
	*/
	func emptyBath() {
		closeAllTaps()
		
		bathWaterAmount = 0.0
		bathWaterTemperature = 0.0
	}
	
	
	private func updateColdAndHotWater() {
		self.severAPI.readFromServerTemperatures { (data, response, error) -> Void in
			if  (response as? NSHTTPURLResponse)?.statusCode == 200 {
				if error == nil {
					self.networkRetries = 0
					let temperatures = self.severAPI.parseToTemperatures(data)
					switch temperatures {
					case (let coldTemperature , _) where coldTemperature == nil:
						println("Do not get Cold Temperature from Server")
					case (_ , let hotTemperature) where hotTemperature == nil:
						println("Do not get Hot Temperature from Server")
					default:
						self.coldWaterTemperature = temperatures.0
						self.hotWaterTemperature = temperatures.1
					}
				}
			} else {
				//count network retries
				self.networkRetries++
			}
		}
	}
	
	func description() -> String {
		return ("Bath amount: \(bathWaterAmount), with temperature: \(bathWaterTemperature), and hot tap open: \(hotTapOpen), and cold tap open: \(coldTapOpen)")
	}
	
	
	func convertCelciusToKelvin(celcius: Double) -> Double {
		return celcius + Double(273.0)
	}
	
	func convertKelvinToCelcius(kelvin: Double) -> Double {
		return kelvin - Double(273.0)
	}
	
	
}
