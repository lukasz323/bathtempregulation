//
//  BathWIthTempRegulationTests.swift
//  BathWIthTempRegulationTests
//
//  Created by Lukasz on 2014-12-04.
//  Copyright (c) 2014 Lukasz Gawryjolek. All rights reserved.
//

import UIKit
import XCTest

class BathWIthTempRegulationTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testEmptyBathWithHotTapOpen() {
		
		let bath = Bath(bathWaterAmount: 0.0, bathWaterTemperature: 0.0, hotWaterTemperature: 50.0, coldWaterTemperature: 10.0)
		
		println("Start test: Bath with amount: \(bath.bathWaterAmount) and temperature: \(bath.bathWaterTemperature)")
		
		bath.hotTapOpen = true
		let timeForOpeningHotTapInMinutes: Double = 1/6	//10sec
		
		// Run the loop
		println("Wait for \(timeForOpeningHotTapInMinutes*60)sec with open hot tap and then check the bath.")
		var hasCalledBack = false
		var loopUntil: NSDate = NSDate(timeIntervalSinceNow:timeForOpeningHotTapInMinutes*60)
		while hasCalledBack == false && (loopUntil.timeIntervalSinceNow > 0) {
			NSRunLoop.currentRunLoop().runMode(NSDefaultRunLoopMode, beforeDate:loopUntil)
		}
		bath.hotTapOpen = false

		XCTAssertEqualWithAccuracy(bath.bathWaterAmount, bath.hotWaterFlow * timeForOpeningHotTapInMinutes, 0.2, "Wrong amount of water in bath with only one tap open: (hot water tap)")
		
		XCTAssertEqualWithAccuracy(bath.bathWaterTemperature, bath.hotWaterTemperature!, 0.1, "Wrong temperature of water in bath with only one tap open: (hot water tap)")
		
    }

	func testEmptyBathWithColdTapOpen() {
		
		let bath = Bath(bathWaterAmount: 0.0, bathWaterTemperature: 0.0, hotWaterTemperature: 50.0, coldWaterTemperature: 10.0)

		println("Start test: Bath with amount: \(bath.bathWaterAmount) and temperature: \(bath.bathWaterTemperature)")
		
		bath.coldTapOpen = true
		let timeForOpeningColdTapInMinutes: Double = 1/6	//10sec
		
		// Run the loop
		println("Wait for \(timeForOpeningColdTapInMinutes*60)sec with open cold tap and then check the bath.")
		var hasCalledBack = false
		var loopUntil: NSDate = NSDate(timeIntervalSinceNow:timeForOpeningColdTapInMinutes*60)
		while hasCalledBack == false && (loopUntil.timeIntervalSinceNow > 0) {
			NSRunLoop.currentRunLoop().runMode(NSDefaultRunLoopMode, beforeDate:loopUntil)
		}
		bath.coldTapOpen = false
		
		XCTAssertEqualWithAccuracy(bath.bathWaterAmount, bath.coldWaterFlow * timeForOpeningColdTapInMinutes, 0.2, "Wrong amount of water in bath with only one tap open: (cold water tap)")
		
		XCTAssertEqualWithAccuracy(bath.bathWaterTemperature, bath.coldWaterTemperature!, 0.1, "Wrong temperature of water in bath with only one tap open: (cold water tap)")
		
	}

	
}
