//
//  ServerAPITests.swift
//  BathWithTempRegulation
//
//  Created by Lukasz on 2014-12-06.
//  Copyright (c) 2014 Lukasz Gawryjolek. All rights reserved.
//

import UIKit
import XCTest

class ServerAPITests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

	func  testServerAPIWithTemperatureData() {
		var hasCalledBack = false
		var serverAPI = ServerAPI()
		var serverData: NSData?
		
		serverAPI.readFromServerTemperatures { (data, response, error) -> Void in
			if (error == nil && (response as? NSHTTPURLResponse)?.statusCode == 200) {
				hasCalledBack = true
				serverData = data
			}
		}
		
		// Run the loop
		println("Wait for sever response to get cold and hot water temperature.")
		var loopUntil: NSDate = NSDate(timeIntervalSinceNow:serverAPI.request.timeoutInterval)
		while hasCalledBack == false && (loopUntil.timeIntervalSinceNow > 0) {
			NSRunLoop.currentRunLoop().runMode(NSDefaultRunLoopMode, beforeDate:loopUntil)
		}

		XCTAssertNotNil(serverData, "Do not get response from server.")
	
	}

}
