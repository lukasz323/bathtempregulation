//
//  ReadMe.h
//  BathWithTempRegulation
//
//  Created by Lukasz on 2014-12-07.
//  Copyright (c) 2014 Lukasz Gawryjolek. All rights reserved.
//

#ifndef BathWithTempRegulation_ReadMe_h
#define BathWithTempRegulation_ReadMe_h


#endif

/*
 TO START APP:
 run local web sever run: sh server.sh  lacated in project directory: ../Swift CODE/BathWIthTempRegulation/json
 which starts python SimpleHTTPServer
 
 And to accelerate process of water coming in bath I recomend change capacity of bath to 15 litres
Bath class #61:  let bathWaterMaxAmount: Double = 15

 
I have written the whole application in new Swift language. I have been learning this new language since it was launched this June.
 It took me around +5h but I am glad with result :-)
 

Before I started writing Swift code I made some research or rather refreshed my memory about “First law of thermodynamics” which says Q1 + Q2 + Q3 = 0 total internal energy of our system (water in the bath, cold water and hot water coming by taps) before and after is the same; does not change, where
Q1 - cold water energy
Q2 - hot water engr.
Q3 - bath with water.
This physical process is nonliteral and continues which means this should be described thanks to derivation and integration equations. This will be quite difficult to implement so I decided to digitalise the process to short repeating amounts of time, here every 100ms I perform the flow and temperature calculation. That still guarantees quite good precision.
See method “calculate” in Bath class.

The application is written in  MVC design pattern with properties and methods descriptions and with Tests Unit methods.

Model contains classes: Bath and ServerAPI

Bath: is a model of our Bath with Taps with all parameters and methods. This class send information to the Controller: ViewController by Notifications - I have chosen this Notification solution due to simplicity and time constrains but I could also use delegates and protocols as well.

ServerAPI - is a class responsible for Server communication and parsing JSON data. This class uses asynchronous server request so the response is received via closure - completion handler block directly to Model. I added a possibly to run application without getting water taps temperature data from server so if you do not have a local test web server it is still possible to play. This behaviour is managed by model - Bath class via counting networkRetries property. If test server is not available instead of temperature for hot and cold water you see "???", taping on tap 3 times switches to simulation mode with set temperatures.

ViewController - is Controller object in MVC responsible mainly for user interaction like tapping on taps or emptying bath, displaying all data and communication to/from Model. Controller uses some animation to make application more fun like:
- rotation and scale of open/close taps
- dial temperature rotation to show bath temperature
- emptying bath animation
- waring when bath is overflow

Unit Tests -  Application has implemented Units Test for checking
- correct calculation of amount of water coming via open tap and its temperature
- connection to the server

For code review perspective I use git during my development so you are welcome to see commits history



Regards
Lukasz Gawryjolek

*/




























*/